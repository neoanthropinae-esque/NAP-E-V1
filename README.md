# Neoanthropinae-esque V1
 A 3D printing project to create an articulated figure using ball joints and segmented parts similarly to polynian. Interchangeability  being the focus.
 
## Getting Started
Download the project on this repository or thingiverse, throw into your slicer of choice. Print. 

GitLlab https://gitlab.com/neoanthropinae-esque/NAP-E-V1

Thingiverse https://www.thingiverse.com/thing:5788850

## Contributing
 If you would like to contribute to the project you are free to download, fork or clone to edit the source files.
 Then either upload, merge request (Figuring this out) or host to gitlab/elsewhere. Citing back to this repository (via Link, mention,  etc.), So others can do the same.

## Images
<details><summary>Click to expand</summary>
 ![alt text](https://gitlab.com/neoanthropinae-esque/NAP-E-V1/-/raw/main/Clover-esque/Refrences/Display%20Images/LampHoldNAP-E.jpg)
 ![alt text](https://gitlab.com/neoanthropinae-esque/NAP-E-V1/-/raw/main/Clover-esque/Refrences/Display%20Images/IMG_20230115_005314-min.jpg)
 ![alt text](https://gitlab.com/neoanthropinae-esque/NAP-E-V1/-/raw/main/Clover-esque/Refrences/Display%20Images/IMG_20230115_002009-min.jpg)
 ![alt text](https://gitlab.com/neoanthropinae-esque/NAP-E-V1/-/raw/main/Clover-esque/Refrences/Display%20Images/ScaleCompressed.jpg)

</details>

## Miscellaneous
 It'll take some time to iron out some of the wrinkles.
 Will populate the wiki with printing, creating and additional information.
